package com.example.demo;

import java.time.LocalDate;
import java.util.UUID;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;


public class Author {

	private UUID id;
	@NotBlank
	public String name;
	public String country;
	public LocalDate dob;
	@Min(value=1)
	public int qtyBooks;
	public Boolean alive;
	
	public Author() {
		
	}
	
	public Author(String name, String country, LocalDate dob, int qtyBooks, Boolean alive) {
		super();

		this.id = UUID.randomUUID();
		this.name = name;
		this.country = country;
		this.dob = dob;
		this.qtyBooks = qtyBooks;
		this.alive = alive;
	}
		
}
