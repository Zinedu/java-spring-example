package com.example.demo;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class AuthorController {

	static int currentID = 1;
	
	@Autowired
	ApplicationCommnadRunner runner;
	
	@GetMapping("/authors")
	public List<Author> GetAllAuthors(){
		
		return AuthorService.queryAuthors();
		
	}
	
	@PostMapping(path = "/addAuthor", consumes = "application/json" )
	public String createAuthor(@RequestBody Author author) {
		
		
		author.dob = LocalDate.parse(author.dob.toString());
		
		if(author.alive.toString() == "true") {
			author.alive = true;	
		}		
		else {
			author.alive = false;
		}
		
		AuthorService.getAuthorList().add(author);
		
		return "Author added";
	}
	
	@GetMapping(path = "/findAuthor/{authorName}")
	public Author FindAuthor(@PathVariable String authorName) {
		
		Author answer = null;
		
		for(int i=0; i<AuthorService.getAuthorList().size() - 1 ;i++) {
			System.out.println(AuthorService.getAuthorList().get(i).name + " | " + authorName);
			if(AuthorService.getAuthorList().get(i).name.toLowerCase().equals(authorName.toLowerCase())) {
				answer = AuthorService.getAuthorList().get(i);
			}
		}
		
		if(answer != null){
			return answer;
		}
		else {
			answer = new Author();
			answer.name = "Author not found";
			return answer;
		}
		
		
	}
	
	@DeleteMapping(path = "/deleteAuthor/{authorName}")
	public String DeleteAuthor(@PathVariable String authorName) {
		String success = "Author not found";
		
		if(AuthorService.DeleteAuthor(authorName)) {
			success = "Author has been found and deleted";
		}
				
		return success;
	}
	
	@PostMapping(path = "/modAuthor/{authorName}")
	public Author EditAuthor(@RequestBody Author author, @PathVariable String authorName) {
		
		if(AuthorService.FindAuthorByName(authorName) != null) {
			AuthorService.ReplaceAuthorValues(author, AuthorService.FindAuthorPositionByName(authorName));
		}
		
		return author;
		
	}
	
	
	
}
