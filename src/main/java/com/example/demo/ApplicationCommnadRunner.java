package com.example.demo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ApplicationCommnadRunner implements CommandLineRunner{
	
	@Autowired
	AuthorService authorService;
	
	@Override
	public void run(String... args) throws Exception {
		AuthorService.AuthorList = new ArrayList<Author>();
		Author author1 = new Author("Mark Dobler", "Germany", LocalDate.of(1992, 3, 17) , 3, true);
		Author author2 = new Author("Kostisch Veushter", "Russia", LocalDate.of(1974, 8, 19), 9, true);
		Author author3 = new Author("Douglas Rickson", "USA", LocalDate.of(1921, 12, 6), 15, false);
		Author author4 = new Author("May Greenfields", "Britain", LocalDate.of(1982, 4, 21) , 5, true);
	
		AuthorService.AuthorList.add(author1);
		AuthorService.AuthorList.add(author2);
		AuthorService.AuthorList.add(author3);
		AuthorService.AuthorList.add(author4);
		
		
	}


}
