package com.example.demo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class AuthorService {
	
	public static List<Author> AuthorList;
	
	public static List<Author> queryAuthors() {
		return AuthorList;
	}
	
	public static boolean DeleteAuthor(String authorName) {
		boolean success = false;
		for(int i = 0; i<AuthorList.size() - 1; i++) {
			if(AuthorList.get(i).name.equals(authorName)) {
				AuthorList.remove(i);
				success = true;
			}
		
	}
		return success;
	}
	
	public static Author FindAuthorByName(String name) {
		for(int i = 0; i<AuthorList.size()-1;i++) {
			if(AuthorList.get(i).name.equals(name)) {
				return AuthorList.get(i);
			}
		}
		return null;
	}
	
	public static int FindAuthorPositionByName(String name) {
		
		for(int i = 0; i<AuthorList.size()-1;i++) {
			if(AuthorList.get(i).name.equals(name)) {
				return i;
			}
		}
		
		return -1;
	}
	
	public static boolean ReplaceAuthorValues(Author author, int authorPosition) {
		
		boolean changes = false;
		
		if(author.alive != null) {
			AuthorList.get(authorPosition).alive = author.alive;
			changes = true;
		}
		if(author.name != null) {
			AuthorList.get(authorPosition).name = author.name;
			changes = true;
		}
		if(author.country != null) {
			AuthorList.get(authorPosition).country = author.country;
			changes = true;
		}
		if(author.dob != null) {
			AuthorList.get(authorPosition).dob = author.dob;
			changes = true;
		}
		if(author.qtyBooks == 0) {
			AuthorList.get(authorPosition).qtyBooks = author.qtyBooks;
			changes = true;
		}
		
		return changes;
	}

	public static List<Author> getAuthorList() {
		return AuthorList;
	}

	public static void setAuthorList(List<Author> authorList) {
		AuthorList = authorList;
	}
	
	
	
}
